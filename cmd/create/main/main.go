package main

import (
	"flag"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"strings"
)

type projectConfig struct {
	DistDir string `yaml:"distDir"`
}

const defaultFuncTemplate string = `package funcer

import (
	"net/http"
)

func %[1]sHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

var %[2]sHandler http.HandlerFunc = %[1]sHandler
`

const defaultModTemplate string = `module funcer.routes/%s

go %d.%d
`

const defaultExportTemplate string = `package router

import (
	f "%s"
)

func init() {
	Routes.Store("%s", f.%sHandler)
}
`

const requireReplace = `
require %[1]s v0.0.0
replace %[1]s => %[2]s`

func main() {
	rootPtr := flag.String("dest", "", "The path to your project root.")
	namePtr := flag.String("name", "", "The name of your new function.")

	flag.Parse()
	if *rootPtr == "" {
		fmt.Fprintf(os.Stderr, "error: %v\n", fmt.Errorf("no rootPath arg set"))
		os.Exit(1)
	}

	funcName := strings.Title(*namePtr)
	if *namePtr != funcName {
		fmt.Println("exporting function for use in router")
	}
	funcImport := fmt.Sprintf("funcer.routes/%s", *namePtr)

	configFilePath := fmt.Sprintf("%s/funcer.yaml", *rootPtr)
	yamlFile, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		fmt.Println(err.Error())
		fmt.Fprintf(os.Stderr, "error: %v\n", fmt.Errorf("could not read config file"))
		os.Exit(1)
	}

	var config projectConfig
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		fmt.Fprintf(os.Stderr, "file reading error: %v\n", fmt.Errorf("config file invalid"))
		os.Exit(1)
	}

	fmt.Printf("creating new func %s\n", funcName)
	err = os.Mkdir(fmt.Sprintf("%s/%s", config.DistDir, *namePtr), 0777)
	if err != nil {
		fmt.Fprintf(os.Stderr, "directory creating error: %v\n", err)
		os.Exit(1)
	}

	routeFile, err := os.Create(fmt.Sprintf("%s/%s/route.go", config.DistDir, *namePtr))
	if err != nil {
		fmt.Fprintf(os.Stderr, "file creating error: %v\n", err)
		os.Exit(1)
	}

	_, err = routeFile.WriteString(fmt.Sprintf(defaultFuncTemplate, *namePtr, funcName))
	if err != nil {
		fmt.Fprintf(os.Stderr, "file writing error: %v\n", err)
		os.Exit(1)
	}

	modFile, err := os.Create(fmt.Sprintf("%s/%s/go.mod", config.DistDir, *namePtr))
	if err != nil {
		fmt.Fprintf(os.Stderr, "file creating error: %v\n", err)
		os.Exit(1)
	}

	_, err = modFile.WriteString(fmt.Sprintf(defaultModTemplate, *namePtr, 1, 11))
	if err != nil {
		fmt.Fprintf(os.Stderr, "file writing error: %v\n", err)
		os.Exit(1)
	}

	exportFile, err := os.Create(fmt.Sprintf("%s/router/%s.go", *rootPtr, *namePtr))
	if err != nil {
		fmt.Fprintf(os.Stderr, "file creating error: %v\n", err)
		os.Exit(1)
	}

	_, err = exportFile.WriteString(
		fmt.Sprintf(
			defaultExportTemplate,
			funcImport,
			funcName,
			funcName,
		),
	)
	if err != nil {
		fmt.Fprintf(os.Stderr, "file writing error: %v\n", err)
		os.Exit(1)
	}

	f, err := os.OpenFile(fmt.Sprintf("%s/go.mod", *rootPtr), os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		fmt.Fprintf(os.Stderr, "file opening error: %v\n", err)
		os.Exit(1)
	}

	defer f.Close()

	if _, err = f.WriteString(
		fmt.Sprintf(
			requireReplace,
			fmt.Sprintf(
				"funcer.routes/%s",
				*namePtr,
			),
			fmt.Sprintf(
				"./handlerFuncs/%s",
				*namePtr,
			),
		),
	); err != nil {
		fmt.Fprintf(os.Stderr, "file writing error: %v\n", err)
		os.Exit(1)
	}
}
